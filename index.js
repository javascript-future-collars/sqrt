function pow(a, b) {
  if (isNaN(a) || isNaN(b)) return "Need numerical values";
  else return Math.pow(a, b);
}
